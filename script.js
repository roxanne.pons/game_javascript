//variables creation (global always on top)
let doorImage1 = document.getElementById('door1');
let doorImage2 = document.getElementById('door2');
let doorImage3 = document.getElementById('door3');
//variables for images behing the doors
let botDoorPath = 'https://s3.amazonaws.com/codecademy-content/projects/chore-door/images/robot.svg';
let beachDoorPath = 'https://s3.amazonaws.com/codecademy-content/projects/chore-door/images/beach.svg';
let spaceDoorPath = 'https://s3.amazonaws.com/codecademy-content/projects/chore-door/images/space.svg';
let closedDoorPath = 'https://s3.amazonaws.com/codecademy-content/projects/chore-door/images/closed_door.svg';
//multiplier for generating random numbers
let numClosedDoors = 3;
let openDoor1;
let openDoor2;
let openDoor3;
//variable with a DOM element 
let startButton = document.getElementById('start');
//so we can then stop the game in case of losing
let currentlyPlaying = true;
//----------------------------------------------------------------------

//arrow function: 
//1. declare new variable const/var/let nameVar
//2. show it's a function = ()
//3. give instructions => {}

//to know if a door has the game-ending choreBot img
const isBot = (door) => {
  if(door.src === botDoorPath) {
    return true;
  } else {
    return false;
  }
}

//so theres no cheat possible: every door only clickable once
const isClicked = (door) => {
  if(door.src === closedDoorPath) {
    return false;
  } else {
    return true;
  }
}

//decrease the number of closed door so we can know when its over (all open)
const playDoor = (door) => {
  numClosedDoors --;
  if(numClosedDoors === 0) {
  gameOver('win');
	} else if(isBot(door)) {
    gameOver('lose');
  }
}

//generate random position for ChoreBot hiding spot
const randomChoreDoorGenerator = () => {
  const choreDoor = Math.floor(Math.random() * numClosedDoors);
  if(choreDoor === 0) {
    openDoor1 = botDoorPath;
    openDoor2 = beachDoorPath;
    openDoor3 = spaceDoorPath;
  } else if(choreDoor === 1) {
    openDoor2 = botDoorPath;
    openDoor1 = beachDoorPath;
    openDoor3 = spaceDoorPath;
  } else{ (choreDoor === 2) 
    openDoor3 = botDoorPath;
    openDoor1 = beachDoorPath;
    openDoor2 = spaceDoorPath;
  }
}

//on click, switch images by replacing doorImage1 by the open one w/ the random img
door1.onclick = () => {
  //in a loop to check if already open -> no cheat
  if(currentlyPlaying && !isClicked(doorImage1)) {
      doorImage1.src = openDoor1;
  		playDoor(doorImage1); //so we can count the number of open doors and know when its over
	}
}
door2.onclick = () => {
  if(currentlyPlaying && !isClicked(doorImage2)) {
      doorImage2.src = openDoor2;
  		playDoor(doorImage2); 
	}
}
door3.onclick = () => {
  if(currentlyPlaying && !isClicked(doorImage3)) {
      doorImage3.src = openDoor3;
  		playDoor(doorImage3); 
	}
}

//reset all variables back to their original value
const startRound = () => {
  numClosedDoors = 3;
  door1.src = closedDoorPath;
  door2.src = closedDoorPath;
  door3.src = closedDoorPath;
  startButton.innerHTML = 'Good luck Friend!';
  currentlyPlaying = true;
  randomChoreDoorGenerator();
}

//make the button start functional
startButton.onclick = () => {
  if(currentlyPlaying === false) {
    startRound(); 
  }
}

//change text in button when game is over
const gameOver = (status) => {
  if(status === 'win') {
    startButton.innerHTML = 'You win bro! Play again?';
  } else {
    startButton.innerHTML = 'Game over! Play again?'
  }
  currentlyPlaying = false;
}

//calling function so its "activated"
startRound();
